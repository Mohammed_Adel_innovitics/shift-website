<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/ 

 
Route::get('PaymentDetalls', function () {
    return view('PaymentDetalls');   
});
Route::get('choose', function () {
    return view('choose');   
});

Route::get('OurFleet', function () {
    return view('OurFleet');   
});

Route::get('ContactUs', function () {
    return view('ContactUs');   
});

Route::get('OurBranch', function () {
    return view('OurBranch');   
});

Route::get('CreateAccount', function () {
    return view('CreateAccount');   
});

Route::get('bookTest', function () {
    return view('bookTest');   
});


Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return view('home');   
});



Route::get('services', function () {
    return view('services');   
});

Route::get('Login', function () {
    return view('Login');   
});

Route::get('Listofbook', function () {
    return view('Listofbook');   
});
  

