<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
   
 @include('header')

@include('navbarBGN')
  

 
 <link href="{{ asset('css/styleCopy.css') }}"   rel='stylesheet' type='text/css'>

 
<div class="tab7 ">
  
  <div class="container-fluid">
    <div class="row">


      <div class="col-md-12">
        <h5 style="font-size: 3em; color: #f7de02;"><strong>Services</strong></h5>
        <hr style="border-color: #f7de02;width: 37px;margin-left: 0px;border-top: 3px solid #f7de02;">
      </div>
      

    </div>
    
  </div>


  <div class="container-fluid">
    <div class="row">


      <div class="col-md-4">
       <p style="color: #eff4fb;">Currently we offer products and services in 2 Key Areas.</p>
      </div>
      

    </div>
    
  </div>

<br>

  <div class="container-fluid">
    <div class="row">


      <div class="col-md-3">
       <img class="image" src="images/s2.png" 
       style="width: 197px;height: auto;margin-left: -45px;"  >

          <p style="color: #f7de02;font-size: 2.3em;">Car</p>
        <p style="color: #f7de02;font-size: 2.3em;"><strong>Rental</strong></p>
        <hr style="border-color: #f7de02;width: 10px;border-top: 3px solid #f7de02;margin-left: 0px;">
        <p style="color: #eff4fb;">Affordable Classical & Fully Automated Car Rental Solutions </p>



      </div>


      <div class="col-md-1"></div>
      <div class="col-md-5">
        <img class="image" src="images/s1.png" style="width: 192px;height: auto;/* margin-left: -20px; */">
         <p style="color: #f7de02;font-size: 2.3em;">Vehicle</p>
        <p style="color: #f7de02;font-size: 2.3em;"><strong>Leasing</strong></p>
        <hr style="border-color: #f7de02;width: 10px;border-top: 3px solid #f7de02;margin-left: 0px;">
        
        <p style="color: #eff4fb;">Tailor made leasing solutions enabled by world class technology, yielding higher control over cost of operations and driving efficiency throughout the organization</p>


      </div>

    </div>
    
  </div>

<!--
<div class="container-fluid">
    <div class="row">

      <div class="col-md-3 c">
        <p>Car</p>
        <p><strong>Rental</strong></p>
        <hr style=" border-color: #f7de02; width: 8px; margin-left: 0px;">
      </div>
      <div class="col-md-3 c">
      <p>Vehicle</p>
        <p><strong>Leasing</strong></p>
        <hr style=" border-color: #f7de02; width: 8px; margin-left: 0px;">
      </div>

    </div>
    
  </div>



-->
<br>

</div>



<div class="tab8">  <img style=" width: 545px; height: 474px;" src="images/arrowsT400.png">  </div>

@include('footer')
 
  