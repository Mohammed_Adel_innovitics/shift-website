 

<div class="footer-bottom">
<div class="container">
					<div class="row">
						<div class="col-sm-6 ">
							<div class="copyright-text">
								 
							</div>
						</div> <!-- End Col -->
						<div class="col-sm-12 col-sm-offset-1">
							 
							<ul class="social-link pull-right">

								<li style="color:white;">find nearby outlets</li>
								<li style="width: 17px;height:  1px;">
									<hr style="transform: rotate(90deg);margin-bottom: 40%;">
								</li>
								<li style="color:white;"> <span style="font-weight:bold;">Call us
								</span>  92003743</li>
								<li style="width: 17px;height:  1px;">
									<hr style="transform: rotate(90deg);margin-bottom: 40%;">
								</li>
								<li><a href=""><img src="images/f.png"></a></li>						
								<li><a href=""><img src="images/t.png"></span></a></li>
								<li><a href=""><img src="images/i.png"></span></a></li>
								 
							</ul>							
						</div> <!-- End Col -->
					</div>
				</div>
</div>

<style type="text/css">
	.footer-bottom {
    padding: 18px 0 12px
    border-top: 1px solid #666;
    /*background: #1e1e1e;*/
}
.copyright-text p {
    color: #ccc;
    margin-top: 9px;
    margin-bottom: 0;
}
.social-link li {
    display: inline-block;
    margin: 0 5px;
}
.social-link li a {
     
     
    text-align: center;
    display: inline-block;
    font-size: 14px;
    transition: .5s;
}
</style>


<script>
	$(document).ready(function () {
	  $('.mobile-nav-button').on('click', function() {
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(1)" ).toggleClass( "mobile-nav-button__line--1");
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(2)" ).toggleClass( "mobile-nav-button__line--2");  
	  $( ".mobile-nav-button .mobile-nav-button__line:nth-of-type(3)" ).toggleClass( "mobile-nav-button__line--3");  
	  
	  $('.mobile-menu').toggleClass('mobile-menu--open');
	  return false;
	}); 
	});
</script>
<!-- //menu --><!-- //navigation -->

<!-- carousal -->
	<script src="{{ asset('js/slick.js') }}" type="text/javascript" charset="utf-8"></script>
	<script type="text/javascript">
		$(document).on('ready', function() {
		  $(".center").slick({
			dots: true,
			infinite: true,
			centerMode: true,
			slidesToShow: 2,
			slidesToScroll: 2,
			responsive: [
				{
				  breakpoint: 768,
				  settings: {
					arrows: true,
					centerMode: false,
					slidesToShow: 2
				  }
				},
				{
				  breakpoint: 480,
				  settings: {
					arrows: true,
					centerMode: false,
					centerPadding: '40px',
					slidesToShow: 1
				  }
				}
			 ]
		  });
		});
	</script>
<!-- //carousal -->

<!-- Gallery -->
	<script type="text/javascript" src="{{ asset('js/simple-lightbox.min.js') }}"></script>
		<script>
			$(function(){
				var gallery = $('.agileinfo-gallery-row a').simpleLightbox({navText:		['&lsaquo;','&rsaquo;']});
			});
		</script> 
	<!-- //Gallery -->

<!-- start-smooth-scrolling -->
<script src="{{ asset('js/SmoothScroll.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- start-smooth-scrolling -->
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->

</body>
<!-- //Body -->
</html>