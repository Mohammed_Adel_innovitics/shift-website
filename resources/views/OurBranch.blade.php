 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="{{ asset('css/r.css') }}"   rel='stylesheet' type='text/css'>
<link href="{{ asset('css/ranaa.css') }}"   rel='stylesheet' type='text/css'>
 @include('header')

@include('navbarBGN') 

 

 <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
 <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
 <div class="tab11">
 	<div class="container-fluid">
 		<div class="row">
 			<div class="col-md-12">
 				<p style="color: #f7de02;"><strong> Show Me</strong></p>
 				<hr style="border-color:  #f7de02; margin-top: 8px;">

 				
 			</div>
 			
 		</div>
 		<div class="row">
 			<div class="col-md-12">
 				<div class="custom-select" style="width:200px;">
                  <select>
                    <option value="" selected ="disabled hidden">Region</option>
                     <option value="1">Egypt</option>
                     <option value="2">Saudi Arabia</option>
                     <option value="3">Jordon</option>
                     <option value="4">Kuwit</option>
   
                  </select>
               </div>
              
 				
 			</div>
 		</div>
 		<div class="row">
 			<div class="col-md-12">
 				<div class="custom-select" style="width:200px;">
                   <select>
                      <option value="" selected disabled hidden>City</option>
                      <option value="1">Cairo</option>
                      <option value="2">Medina</option>
                     <option value="3">Jeddah</option>
                   
                    </select>
               </div>
              
 				
 			</div>
 		</div>
 		<div class="row">
 			<div class="col-md-12">
 				<div class="custom-select" style="width:200px;">
                   <select>
                   	 <option value="" selected disabled hidden>Classical or Automated</option>
                      
                      <option value="1">Classical</option>
                      <option value="2">Aoutomated</option>
                 
                   
                    </select>
               </div>
              
 				
 			</div>
 		</div>
 		<div class="row">
 			<div class="col-md-12">
 				<div class="custom-select" style="width:200px;">
                   <select>
                     <option value="" selected ="hidden" >Classical or Automated</option>
                      <option value="1">Classical</option>
                      <option value="2">Aoutomated</option>
                 
                   
                    </select>
               </div>
              
 				
 			</div>
 		</div>
 		
 	</div>
 	<br>
 	<br>
 </div>

<div class="tab12">
<div class="container-fluid">
	<div class="row">
		<div class="col-md-7 col-sm-5">
			<h4 style="font-size: 24px; ">Our</h4>
            <p style="font-size: 24px;font-weight: 900; ">Branches</p>
			
		</div>
		 
		
	</div>
	
</div>
</div>

<div class="tab9">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8" style=" margin-left: -40px;">
				<img  src="images/arrowsLogin.png" width=" 95%" height="auto">
			</div>
		</div>
	</div>
</div>



<div class="tab10">
	 <div id="map" style="width:100%;height:-webkit-fill-available;"></div>
</div>



<script>
function myMap() {
  var mapCanvas = document.getElementById("map");
  var myCenter = new google.maps.LatLng(23.8859,45.0792); 
  var mapOptions = {center: myCenter, zoom: 4};
  var map = new google.maps.Map(mapCanvas,mapOptions);
  var marker = new google.maps.Marker({
    position: myCenter,
    animation: google.maps.Animation.BOUNCE
  });
  marker.setMap(map);
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyARpAi7X0reLbVasoaGiTMyLgPbcQBJqZk&callback=myMap"></script>







<script>
var x, i, j, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
for (i = 0; i < x.length; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < selElmnt.length; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /*when an item is clicked, update the original select box,
        and the selected item:*/
        var y, i, k, s, h;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        h = this.parentNode.previousSibling;
        for (i = 0; i < s.length; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            for (k = 0; k < y.length; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
      /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
      e.stopPropagation();
      closeAllSelect(this);
      this.nextSibling.classList.toggle("select-hide");
      this.classList.toggle("select-arrow-active");
    });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x, y, i, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  for (i = 0; i < y.length; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < x.length; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);
</script>





<style type="text/css">
  /*the container must be positioned relative:*/
.custom-select {
  position: relative;
  font-family: Arial;
}
.custom-select select {
  display: none; /*hide original SELECT element:*/
}
.select-selected {
 
    background-color: #272a67;
}
/*style the arrow inside the select element:*/
.select-selected:after {
  position: absolute;
  content: "";
  top: 14px;
  right: 10px;
  width: 0;
  height: 0;
  border: 6px solid transparent;
  border-color: #fff transparent transparent transparent;
}
/*point the arrow upwards when the select box is open (active):*/
.select-selected.select-arrow-active:after {
  border-color: transparent transparent #fff transparent;
  top: 7px;
}
/*style the items (options), including the selected item:*/
.select-items div,.select-selected {
  color: #ffffff;
    padding: 6px 1px;
    border: 1px solid transparent;
    border-color: transparent transparent #ffffff transparent;
    cursor: pointer;
    user-select: none;
    margin-top: 11px;
}
/*style items (options):*/
.select-items {
    position: absolute;
    background-color: #272a67;
    top: 100%;
    left: 0;
    right: 0;
    z-index: 99;
}
/*hide the items when the select box is closed:*/
.select-hide {
  display: none;
}
.select-items div:hover, .same-as-selected {
  background-color: rgba(0, 0, 0, 0.1);
}


</style>